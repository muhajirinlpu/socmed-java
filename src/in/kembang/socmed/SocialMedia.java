package in.kembang.socmed;

import in.kembang.socmed.relation.Relation;
import in.kembang.socmed.relation.RelationStorage;
import in.kembang.socmed.relation.Relationable;

import java.util.Arrays;
import java.util.LinkedList;

public class SocialMedia implements RelationStorage {
    LinkedList<User> users = new LinkedList<>();
    LinkedList<Relation> relations = new LinkedList<>();

    private static SocialMedia instance;

    public static SocialMedia getInstance() {
        if (instance == null) {
            instance = new SocialMedia();
        }

        return instance;
    }

    public User getUser(String name) {
        return this.users.stream().filter(user -> user.name.equals(name)).findFirst().orElse(null);
    }

    public User registerUser(String username) {
        User user = this.getUser(username);

        // prevent duplicate user
        if (user == null) {
            user = new User(username);
            this.users.add(user);
        }

        return user;
    }

    Relation getRelation(User user, User otherUser) {
        return this.relations
                .stream()
                .filter(relation -> (Arrays.stream(relation.relationables)).allMatch(u -> u.getIdentifier().equals(user.name) || u.getIdentifier().equals(otherUser.name) ))
                .findFirst()
                .orElse(null);
    }

    public void removeRelation(Relation relation) {
        this.relations.remove(relation);
    }

    Relation[] getRelationsByUser(User user) {
        return this.relations
                .stream()
                .filter(relation -> Arrays.stream(relation.relationables).anyMatch(c -> c.getIdentifier().equals(user.name)))
                .toArray(Relation[]::new);
    }

    @Override
    public void addRelation(Relationable relationable, Relationable candidate, Relation.FriendType type) {
        Relation relation = this.getRelation((User) relationable, (User) candidate);

        if (relation == null) {
            this.relations.add(new Relation(relationable, candidate, type));
            return;
        }

        relation.type = type;
    }
}
