package in.kembang.socmed;

import in.kembang.socmed.relation.Relation;
import in.kembang.socmed.relation.Relationable;

import java.util.Arrays;

public class User implements Relationable {

    @Override
    public String getIdentifier() {
        return this.name;
    }

    static class Friend {
        User cursor, user;
        Relation.FriendType type;

        Friend(User cursor, User user, Relation.FriendType type) {
            this.cursor = cursor;
            this.user = user;
            this.type = type;
        }

        void setRelationType(Relation.FriendType type) {
            this.type = type;
            SocialMedia.getInstance().getRelation(this.cursor, this.user).type = type;
        }

        public User getUser() {
            return user;
        }

        void removeFriend() {
            SocialMedia.getInstance().removeRelation(SocialMedia.getInstance().getRelation(this.cursor, this.user));
        }
    }

    String name;

    User(String name) {
        this.name = name;
    }

    public User addFriend(String nameCandidate, Relation.FriendType type) {
        SocialMedia socialMedia = SocialMedia.getInstance();
        User user = socialMedia.getUser(nameCandidate);

        // force create user while user is not available
        if (user == null) {
            user = socialMedia.registerUser(nameCandidate);
        }

        socialMedia.addRelation(this, user, type);

        return this;
    }

    public Friend[] getFriends() {
        return Arrays.stream(SocialMedia.getInstance().getRelationsByUser(this))
                .map(relation -> new Friend(
                        this,
                        (User) Arrays.stream(relation.relationables).filter(user -> !((User) user).name.equals(this.name))
                                .findFirst().orElse(null),
                        relation.type
                )).toArray(Friend[]::new);
    }

    public Friend getFriend(String name) {
        User user = SocialMedia.getInstance().getUser(name);
        Relation relation = SocialMedia.getInstance().getRelation(this, user);

        return new Friend(this, user, relation.type);
    }

    @Override
    public String toString() {
        return "in.kembang.socmed.User{" +
                "name='" + name + '\'' +
                ",friends=" +
                Arrays.toString(Arrays.stream(this.getFriends())
                        .map(friend -> "(" + friend.user.name + "[" + friend.type + "])")
                        .toArray(String[]::new)) +
                '}';
    }
}
