package in.kembang.socmed;

import in.kembang.socmed.relation.Relation;

import java.util.Arrays;

public class Test {
    static void printRelationshipStatus() {
        System.out.println("######## PRINT ALL USERS AND ITS FRIENDS");
        SocialMedia.getInstance().users.forEach(user -> {
            System.out.println(user.toString());
        });

        System.out.println("######## PRINT ALL USERS THAT DOESN'T HAVE CLOSE FRIEND");
        SocialMedia.getInstance().users.forEach(user -> {
            if (Arrays.stream(user.getFriends()).noneMatch(friend -> friend.type == Relation.FriendType.CLOSE_FRIEND)) {
                System.out.println(user);
            }
        });

    }

    public static void main(String[] args) {
        SocialMedia socialMedia = SocialMedia.getInstance();

        socialMedia.registerUser("Melly")
                .addFriend("Once", Relation.FriendType.ACQUAINTANCE)
                .addFriend("Pitra", Relation.FriendType.ACQUAINTANCE)
                .addFriend("Ratu", Relation.FriendType.CLOSE_FRIEND)
                .addFriend("Seftyan", Relation.FriendType.FRIEND);
        socialMedia.registerUser("Once")
                .addFriend("Niki", Relation.FriendType.CLOSE_FRIEND)
                .addFriend("Melly", Relation.FriendType.ACQUAINTANCE)
                .addFriend("Quraish", Relation.FriendType.FRIEND);
        socialMedia.registerUser("Tio")
                .addFriend("Umai", Relation.FriendType.FRIEND)
                .addFriend("Ratu", Relation.FriendType.CLOSE_FRIEND)
                .addFriend("Venita", Relation.FriendType.CLOSE_FRIEND);
        socialMedia.registerUser("Venita")
                .addFriend("Pitra", Relation.FriendType.CLOSE_FRIEND)
                .addFriend("Tio", Relation.FriendType.CLOSE_FRIEND)
                .addFriend("Quraish", Relation.FriendType.FRIEND);
        socialMedia.registerUser("Umai")
                .addFriend("Niki", Relation.FriendType.FRIEND)
                .addFriend("Tio", Relation.FriendType.FRIEND)
                .addFriend("Seftyan", Relation.FriendType.CLOSE_FRIEND);

        printRelationshipStatus();

        System.out.println("\n\n> RUNNING CHANGES FOR RELATIONSHIP USER\n\n");
        socialMedia.getUser("Seftyan").getFriend("Umai").removeFriend();
        socialMedia.getUser("Pitra").getFriend("Melly").setRelationType(Relation.FriendType.FRIEND);
        socialMedia.getUser("Niki").getFriend("Once").getUser().addFriend("Umai", Relation.FriendType.ACQUAINTANCE);

        printRelationshipStatus();
    }
}
