package in.kembang.socmed.relation;

public interface RelationStorage {
    void addRelation(Relationable relationable, Relationable candidate, Relation.FriendType type);

    void removeRelation(Relation relation);
}
