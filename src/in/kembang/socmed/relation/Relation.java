package in.kembang.socmed.relation;

public class Relation {
    public enum FriendType {
        CLOSE_FRIEND,
        FRIEND,
        ACQUAINTANCE
    }

    public Relationable[] relationables = new Relationable[2];
    public FriendType type;

    public Relation(Relationable relationable, Relationable otherRelationable, FriendType friendType) {
        this.relationables[0] = relationable;
        this.relationables[1] = otherRelationable;
        this.type = friendType;
    }
}
