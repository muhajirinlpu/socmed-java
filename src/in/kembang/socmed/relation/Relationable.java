package in.kembang.socmed.relation;

public interface Relationable {
    public String getIdentifier();
}
